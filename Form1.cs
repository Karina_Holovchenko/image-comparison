using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_comp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp;)| *.jpg; *.jpeg; *.gif; *.bmp;";
            if (open.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = new Bitmap(open.FileName);
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp;)| *.jpg; *.jpeg; *.gif; *.bmp;";
            if (open.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.Image = new Bitmap(open.FileName);
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null || pictureBox2.Image == null)
            {
                MessageBox.Show("Please select an image");
                return;
            }
            else
            {
                Bitmap bmp1 = (Bitmap)pictureBox1.Image;
                Bitmap bmp2 = (Bitmap)pictureBox1.Image;
                int[] histogram_1 = new int[256];
                int[] histogram_2 = new int[256];
                float max = 0;
                float[] diff = new float[256];

                //гистограмма 1
                for (int i = 0; i < bmp1.Width; i++)
                {
                    for (int j = 0; j < bmp1.Height; j++)
                    {
                        //int redValue = bmp.GetPixel(i, j).R;  //только для R
                        int rgbValue = (int)((0.2126 * bmp1.GetPixel(i, j).R + 0.7152 * bmp1.GetPixel(i, j).G + 0.0722 * bmp1.GetPixel(i, j).B));
                        histogram_1[rgbValue]++;
                        if (max < histogram_1[rgbValue])
                            max = histogram_1[rgbValue];
                    }
                }
                //гистограмма 2
                for (int i = 0; i < bmp2.Width; i++)
                {
                    for (int j = 0; j < bmp2.Height; j++)
                    {
                        int rgbValue = (int)((0.2126 * bmp2.GetPixel(i, j).R + 0.7152 * bmp2.GetPixel(i, j).G + 0.0722 * bmp2.GetPixel(i, j).B));
                        histogram_2[rgbValue]++;
                        if (max < histogram_2[rgbValue])
                            max = histogram_2[rgbValue];
                    }
                }
                for (int i = 0; i < 256; i++)
                {
                    diff[i] = ((float)Math.Abs(histogram_1[i] - histogram_2[i])) / 255;
                }

                //на сколько % отличаются
                float degreeOfDiff = diff.Sum() / histogram_1.Length * 100;
                bool sameDistribution = degreeOfDiff == 0;

                if(degreeOfDiff < 10)
                    textBox1.Text = "Різні";
                else
                    textBox1.Text = "Схожі";

            }
        }
    }
}
